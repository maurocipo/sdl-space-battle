#ifndef PARTICLE_PRIMITIVE_H
#define PARTICLE_PRIMITIVE_H

#include <stdint.h>

class Particle
{
public:
	Particle(const int PosX, const int PosY);
	~Particle();

	void setX(const int X);
	void setY(const int Y);
	void addFrame();
	void setFrame(const int Frame);

	int getX() const;
	int getY() const;
	uint8_t getFrame() const;

private:
	// Position
	int mPosX, mPosY;

	// Current Frame of Explosion
	uint8_t mFrame;
};
#endif