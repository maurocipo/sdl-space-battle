#include "ObjectPrimitives.h"
#include "Auxiliary.h"
#include "iostream"

Primitive::Primitive(const int X, const int Y, const int VelX, const int VelY, const uint8_t TextureNumber)
	: mPosX(X), mPosY(Y), mVelX(VelX), mVelY(VelY), mFace(1), mTextureNumber(TextureNumber), mAliade(false),
	mCircularMovement(false)
{
}

Primitive::Primitive(const int X, const int Y, const int VelX, const int VelY, const uint8_t TextureNumber,
	const bool aliade)
	: mPosX(X), mPosY(Y), mVelX(VelX), mVelY(VelY), mFace(1), mTextureNumber(TextureNumber), mAliade(aliade)
{
	if (!mAliade && TextureNumber < 2)
		if ((double)rand() / (RAND_MAX) > 0.5)
		{
			mCircularMovement = true;
			mVelX = 5;
		}
		else
			mCircularMovement = false;
	else
		mCircularMovement = false;

	mTicks = 0;
}

Primitive::~Primitive()
{
}


bool Primitive::move()
{
	++mTicks;
	//Move the ship left or right
	if (!mAliade){
		if (mCircularMovement){
			if (mPosY > (SCREEN_HEIGHT / 8)){
				mPosX += (int)(sin(mTicks * 3.14159 / 40) * mVelX);
				//mPosY += (int) (cos(mTicks * 3.14159 / 40) * mVelY);
				mPosY += 4;
			}
			else{
				//Move the enemy up or down
				mPosY += mVelY;
			}

		}
		else{
			//Move the enemy up or down
			mPosY += mVelY;
			//Move the enemy right or left
			mPosX += mVelX;
		}

		//If the enemy went too far to the left or right
		if ((mPosX < 0) || (mPosX + 10 > SCREEN_WIDTH))
		{
			//if (mCircularMovement)
			//Move back
			//mPosX -= mVelX;
			//return true;
		}

		//If the enemy went too far up or down
		if ((mPosY < -mHeight) || (mPosY > SCREEN_HEIGHT + mHeight))
		{
			//Move back
			mPosY -= mVelY;
			return true;
		}

		return false;
	}
	else{
		mPosX += mVelX;
		//If the aliade went too far to the left or right
		if ((mPosX < 0) || (mPosX > SCREEN_WIDTH - mWith))
		{
			//Move back
			mPosX -= mVelX;
			return true;
		}

		//Move the aliade up or down
		mPosY += mVelY;
		//If the aliade went too far up or down
		if ((mPosY < 0) || (mPosY > SCREEN_HEIGHT - mHeight))
		{
			//Move back
			mPosY -= mVelY;
			return true;
		}

		return false;
	}
}

bool Primitive::checkCollision(Primitive& a) const{
	int topA = a.getY();
	int topB = mPosY;
	int bottomA = a.getY() + a.getHeight();
	int bottomB = mPosY + mHeight;
	int leftA = a.getX();
	int leftB = mPosX;
	int rightA = a.getX() + a.getWidth();
	int rightB = mPosX + mWith;

	if (topA > bottomB) return false;
	if (leftA > rightB) return false;
	if (rightA < leftB) return false;
	if (bottomA < topB) return false;

	return true;
}

int Primitive::getX() const{
	return mPosX;
}

int Primitive::getY() const{
	return mPosY;
}

uint8_t Primitive::getFace() const{
	return mFace;
}

uint8_t Primitive::getTexNum()const{
	return mTextureNumber;
}

int Primitive::getWidth() const{
	return mWith;
}

int Primitive::getHeight() const{
	return mHeight;
}

int Primitive::getVelX() const{
	return mVelX;
}

int Primitive::getVelY() const{
	return mVelY;
}

void Primitive::setX(const int X){
	mPosX = X;
}

void Primitive::setY(const int Y){
	mPosY = Y;
}

void Primitive::setFace(const uint8_t Face){
	mFace = Face;
}

void Primitive::setWidth(const int width){
	mWith = width;
}

void Primitive::setHeight(const int height){
	mHeight = height;
}

void Primitive::setVelX(const int velX){
	mVelX = velX;
}

void Primitive::setVelY(const int velY){
	mVelY = velY;
}