#ifndef SMOKE_PARTICLE_H
#define SMOKE_PARTICLE_H

#include "ParticlePrimitive.h"

class SmokeParticle : public Particle
{
public:
	SmokeParticle(const int PosX, const int PosY);
	~SmokeParticle();
};
#endif