#ifndef EXPLOSION_H
#define EXPLOSION_H

#include "ParticlePrimitive.h"

class Explosion : public Particle
{
public:
	Explosion(const int PosX, const int PosY);
	~Explosion();
};
#endif