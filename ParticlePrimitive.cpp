#include "ParticlePrimitive.h"

Particle::Particle(const int PosX, const int PosY) : mPosX(PosX), mPosY(PosY), mFrame(0)
{
}

Particle::~Particle()
{
}

void Particle::setX(const int X){
	mPosX = X;
}

void Particle::setY(const int Y){
	mPosY = Y;
}

void Particle::addFrame(){
	mFrame += 1;
}

void Particle::setFrame(const int Frame){
	mFrame = Frame;
}

int Particle::getX() const{
	return mPosX;
}

int Particle::getY() const{
	return mPosY;
}

uint8_t Particle::getFrame() const{
	return mFrame;
}