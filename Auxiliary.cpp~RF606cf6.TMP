#include "Auxiliary.h"

#include <iostream>
#include <random>

int EnemyCreationTimes[10] = {};

bool SDLInit(SDL_Window **win, SDL_Renderer **ren, TTF_Font **gFont){
	// Initialize SDL subsystems to use.
	if (SDL_Init(SDL_INIT_VIDEO) != 0){
		logSDLError(std::cout, "SDL_Init ");
		return false;
	}

	// Initialize SDL_Image
	if ((IMG_Init(IMG_INIT_PNG) & IMG_INIT_PNG) != IMG_INIT_PNG){
		logSDLError(std::cout, "IMG_Init");
		SDL_Quit();
		return false;
	}

	// Initialize Font
	if (TTF_Init() == -1)
	{
		printf("SDL_ttf could not initialize! SDL_ttf Error: %s\n", TTF_GetError());
		return false;
	}
	else
	{
		//Open the font
		*gFont = TTF_OpenFont("fonts/fedservice.ttf", 30);
		if (*gFont == NULL)
		{
			printf("Failed to load lazy font! SDL_ttf Error: %s\n", TTF_GetError());
		}
	}

	// Open a window.
	*win = SDL_CreateWindow("Hello World!", 100, 100, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
	if (win == nullptr){
	logSDLError(std::cout, "SDL_CreateWindow ");
	SDL_Quit();
	return false;
	}

	// Create a renderer to draw the window.
	*ren = SDL_CreateRenderer(*win, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
	if (ren == nullptr){
	SDL_DestroyWindow(*win);
	logSDLError(std::cout, "SDL_CreateRenderer ");
	SDL_Quit();
	return false;
	}

	return true;
}

void logSDLError(std::ostream &os, const std::string &msg){
	os << msg << "Error: " << SDL_GetError() << std::endl;
}

SDL_Texture* loadTexture(const char* file, SDL_Renderer *ren){
	SDL_Texture *texture = nullptr;
	
	//Load image at specified path
	SDL_Surface* loadedSurface = IMG_Load(file);
	if (loadedSurface == NULL)
	{
		printf("Unable to load image %s! SDL_image Error: %s\n", file, IMG_GetError());
		SDL_Delay(10000);
		exit(1);
	}
	else
	{
		//Create texture from surface pixels
		texture = SDL_CreateTextureFromSurface(ren, loadedSurface);
		if (texture == NULL)
		{
			printf("Unable to create texture from %s! SDL Error: %s\n", file, SDL_GetError());
			SDL_Delay(10000);
			exit(1);
		}

		//Get rid of old loaded surface
		SDL_FreeSurface(loadedSurface);
	}

	return texture;
}

void renderTexture(SDL_Texture *tex, SDL_Renderer *ren, int x, int y, SDL_Rect *clip = nullptr){
	//Setup the destination rectangle to be at the position we want
	SDL_Rect dst;
	dst.x = x;
	dst.y = y;

	if (clip != nullptr){
		dst.w = clip->w;
		dst.h = clip->h;
	}
	else {
		SDL_QueryTexture(tex, NULL, NULL, &dst.w, &dst.h);
	}

	SDL_RenderCopy(ren, tex, clip, &dst);
}

void loadAliadeTex(SDL_Texture **top, SDL_Texture **left, SDL_Texture **right, SDL_Renderer *ren){
	*top = loadTexture("imgs/shipT.png",ren);
	*left = loadTexture("imgs/shipL.png", ren);
	*right = loadTexture("imgs/shipR.png", ren);
}

void loadTexs(std::vector<SDL_Texture*> &EnemiesTexs, std::vector<SDL_Texture*> &BulletsTexs, 
	SDL_Renderer *ren){
	//Load Enemies
	EnemiesTexs.push_back(loadTexture("imgs/enemy2.png", ren));
	EnemiesTexs.push_back(loadTexture("imgs/enemy1.png",ren));
	EnemiesTexs.push_back(loadTexture("imgs/enemy3.png", ren));
	EnemiesTexs.push_back(loadTexture("imgs/enemy4.png", ren));
	EnemiesTexs.push_back(loadTexture("imgs/enemy5.png", ren));

	//Load Bullet1
	BulletsTexs.push_back(loadTexture("imgs/littleMissile.png", ren));	
}

bool moveShipsAndBullets(std::vector<Ship*>& VEnemies, std::vector<Bullet*>& VBullets, std::vector<Explosion*>& VExplosions,
	Ship& aliade, uint16_t& kills){
	
	// Vector that contains iterators from VBullets with ships to destroy
	std::vector<int> VBDestroyer;
	// Vector that contains iterators from VEnemies with ships to destroy
	std::vector<int> VDestroyer;

	// Position of the plane to erase
	int pPosition = 0;
	// Position of the bullet to erase
	int bPosition = 0;
	
	// Return if aliade crashed
	bool crash = false;

	// Auxiliary to check if bullet hitted
	bool hit = false;

	aliade.move();

	for (std::vector<Ship*>::iterator it = VEnemies.begin(); it != VEnemies.end(); ++it){
		// Moves and if the ship went out of limits, destroy it.
		if ((*it)->move()){
			VDestroyer.insert(VDestroyer.begin(),pPosition);
		}
		else{
			// If a ship collides with the aliade, quit one life.
			if ((*it)->checkCollision(aliade)) crash = true;
		}
		++pPosition;
	}
	
	// Destroy crashed ships and destroy its textures.
	for (std::vector<int>::iterator it = VDestroyer.begin(); it != VDestroyer.end(); ++it){
		delete(*(VEnemies.begin() + (*it)));
		VEnemies.erase(VEnemies.begin() + (*it));
	}
	// Clear vector with elements to destroy, since we destroyed them all.
	VDestroyer.clear();

	
	// Move bullets and check collision with ships.
	for (std::vector<Bullet*>::iterator it = VBullets.begin(); it != VBullets.end(); ++it){
		pPosition = 0;
		// If a bullet collides with a ship, add them to their respective destructors.
		// If not, move it and check if it went out of limits
		for (std::vector<Ship*>::iterator itS = VEnemies.begin(); itS != VEnemies.end(); ++itS){
			if ((*it)->checkCollision(*(*itS))){
				VBDestroyer.insert(VBDestroyer.begin(), bPosition);
				hit = true;
				// quitLife returns true if the ship is killed.
				if ((*itS)->quitLife((*it)->getPower())){
					VDestroyer.insert(VDestroyer.begin(), pPosition);
					kills += 1;
					VExplosions.push_back(new Explosion((*itS)->getX(), (*itS)->getY()));
					// 128 is the height of the explosion and 4 is a magic number to add more explosions.
					for (uint8_t i = 0; i < (((*itS)->getHeight() / 128)*4);++i){
						VExplosions.push_back(new Explosion((*itS)->getX() + rand() % (*itS)->getWidth(),
							(*itS)->getY() + rand() % (*itS)->getHeight()));
					}
					
				}
				break;
			}
			++pPosition;
		}

		if ((!hit) && (*it)->move()){
			VBDestroyer.insert(VBDestroyer.begin(), bPosition);
			hit = false;
		}
		++bPosition;
	}

	// Destroy crashed ships and destroy its textures.
	for (std::vector<int>::iterator it = VDestroyer.begin(); it != VDestroyer.end(); ++it){
		if (*it == VEnemies.size()){
			VEnemies.pop_back();
		}
		else{
			delete(*(VEnemies.begin() + *it));
			VEnemies.erase(VEnemies.begin() + *it);
		}
	}
	// Destroy bullets and destroy its textures.
	for (std::vector<int>::iterator it = VBDestroyer.begin(); it != VBDestroyer.end(); ++it){

		if (*it == VBullets.size()){
			VBullets.pop_back();
		}
		else{
			delete(*(VBullets.begin() + *it));
			VBullets.erase(VBullets.begin() + *it);
		}
	}
	return crash;
}

void createEnemies(uint8_t level, SDL_Renderer *ren, std::vector<Ship*>& VEnemies, std::vector<SDL_Texture*> &EnemiesTexs){
	
	int height, width, initialX, initialY, velX, velY, textureNumber, life;

	switch (level)
	{
	case 1:{
		textureNumber = 1;
		life = 60;
		if ((SDL_GetTicks() - EnemyCreationTimes[textureNumber]) > 500){
			velX = 2;
			velY = 4;
			SDL_QueryTexture(EnemiesTexs.at(textureNumber), NULL, NULL, &width, &height);
			initialX = rand() % (SCREEN_WIDTH - width);
			initialY = -height;
			VEnemies.push_back(new Ship(initialX, initialY, velX, velY, life, textureNumber, false));
			VEnemies.back()->setHeight(height);
			VEnemies.back()->setWidth(width);
			EnemyCreationTimes[textureNumber] = SDL_GetTicks();
		}

		break;
	}
	case 2:{
		textureNumber = 0;
		life = 10;
		if ((SDL_GetTicks() - EnemyCreationTimes[textureNumber]) > 200){
			velX = 0;
			velY = 5;
			SDL_QueryTexture(EnemiesTexs.at(textureNumber), NULL, NULL, &width, &height);
			initialX = rand() % (SCREEN_WIDTH - width);
			initialY = -height;
			VEnemies.push_back(new Ship(initialX, initialY, velX, velY, life, textureNumber, false));
			VEnemies.back()->setHeight(height);
			VEnemies.back()->setWidth(width);
			EnemyCreationTimes[textureNumber] = SDL_GetTicks();
		}

		textureNumber = 3;
		life = 450;
		if ((SDL_GetTicks() - EnemyCreationTimes[textureNumber]) > 10000){
			velX = 0;
			velY = 2;
			SDL_QueryTexture(EnemiesTexs.at(textureNumber), NULL, NULL, &width, &height);
			initialX = rand() % (SCREEN_WIDTH - width);
			initialY = -height;
			VEnemies.push_back(new Ship(initialX, initialY, velX, velY, life, textureNumber, false));
			VEnemies.back()->setHeight(height);
			VEnemies.back()->setWidth(width);
			EnemyCreationTimes[textureNumber] = SDL_GetTicks();
		}
		break;
	}
	case 3:{
		textureNumber = 1;
		life = 60;
		if ((SDL_GetTicks() - EnemyCreationTimes[textureNumber]) > 500){
			velX = 0;
			velY = 5;
			SDL_QueryTexture(EnemiesTexs.at(textureNumber), NULL, NULL, &width, &height);
			initialX = rand() % (SCREEN_WIDTH - width);
			initialY = -height;
			VEnemies.push_back(new Ship(initialX, initialY, velX, velY, life, textureNumber, false));
			VEnemies.back()->setHeight(height);
			VEnemies.back()->setWidth(width);
			EnemyCreationTimes[textureNumber] = SDL_GetTicks();
		}

		textureNumber = 3;
		life = 450;
		if ((SDL_GetTicks() - EnemyCreationTimes[textureNumber]) > 10000){
			velX = 0;
			velY = 2;
			SDL_QueryTexture(EnemiesTexs.at(textureNumber), NULL, NULL, &width, &height);
			initialX = rand() % (SCREEN_WIDTH - width);
			initialY = -height;
			VEnemies.push_back(new Ship(initialX, initialY, velX, velY, life, textureNumber, false));
			VEnemies.back()->setHeight(height);
			VEnemies.back()->setWidth(width);
			EnemyCreationTimes[textureNumber] = SDL_GetTicks();
		}

		textureNumber = 0; 
		life = 10;
		if ((SDL_GetTicks() - EnemyCreationTimes[textureNumber]) > 200){
			velX = 0;
			velY = 4;
			SDL_QueryTexture(EnemiesTexs.at(textureNumber), NULL, NULL, &width, &height);
			initialX = rand() % (SCREEN_WIDTH - width);
			initialY = -height;
			VEnemies.push_back(new Ship(initialX, initialY, velX, velY, life, textureNumber, false));
			VEnemies.back()->setHeight(height);
			VEnemies.back()->setWidth(width);
			EnemyCreationTimes[textureNumber] = SDL_GetTicks();
		}

		textureNumber = 2;
		life = 60;
		if ((SDL_GetTicks() - EnemyCreationTimes[textureNumber]) > 2000){
			velX = 0;
			velY = 3;
			SDL_QueryTexture(EnemiesTexs.at(textureNumber), NULL, NULL, &width, &height);
			initialX = rand() % (SCREEN_WIDTH - width);
			initialY = -height;
			VEnemies.push_back(new Ship(initialX, initialY, velX, velY, life, textureNumber, false));
			VEnemies.back()->setHeight(height);
			VEnemies.back()->setWidth(width);
			EnemyCreationTimes[textureNumber] = SDL_GetTicks();
		}
		break;
	}

	default:
		break;
	}
}

void loadFromRenderedText(std::string textureText, TTF_Font **gFont, SDL_Texture **Text, SDL_Renderer** ren)
{
	
	SDL_Color textColor = { 0xFF, 0x00, 0x00 };
	//Render text surface
	SDL_Surface* textSurface = TTF_RenderText_Solid(*gFont, textureText.c_str(), textColor);
	if (textSurface == NULL)
	{
		printf("Unable to render text surface! SDL_ttf Error: %s\n", TTF_GetError());
	}
	else
	{
		//Create texture from surface pixels
		*Text = SDL_CreateTextureFromSurface(*ren, textSurface);
		if (Text == NULL)
		{
			printf("Unable to create texture from rendered text! SDL Error: %s\n", SDL_GetError());
		}
/*		else
		{
			//Get image dimensions
			mWidth = textSurface->w;
			mHeight = textSurface->h;
		}
*/
		//Get rid of old surface
		SDL_FreeSurface(textSurface);
	}

}

void loadExplosionSprite(SDL_Rect (&exSpriteClips)[49]){
	
	for (uint8_t i = 0; i < 7; ++i){
		for (uint8_t j = 0; j < 7; ++j){

			exSpriteClips[i].x = j * 128;
			exSpriteClips[i].y = i * 128;
			exSpriteClips[i].h = 128;
			exSpriteClips[i].w = 128;

		}
	}

}