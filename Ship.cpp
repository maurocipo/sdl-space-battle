#include "Auxiliary.h"
#include "Ship.h"

#include <iostream>

Ship::Ship(const int X, const int Y, const int VelX, const int VelY, int16_t Life, uint8_t TextureNumber, bool aliade)
	: Primitive(X, Y, VelX, VelY, TextureNumber, aliade)
{
	mLife = Life;
}

Ship::~Ship()
{
}

bool Ship::handleEvent(SDL_Event& e)
{
	//If a key was pressed
	if (e.type == SDL_KEYDOWN && e.key.repeat == 0)
	{
		//Adjust the velocity
		switch (e.key.keysym.sym)
		{
		case SDLK_UP:		setVelY(getVelY() - VEL); break;
		case SDLK_DOWN:		setVelY(getVelY() + VEL); break;
		case SDLK_LEFT:		setVelX(getVelX() - VEL); break;
		case SDLK_RIGHT:	setVelX(getVelX() + VEL); break;
		}
	}
	//If a key was released
	else if (e.type == SDL_KEYUP && e.key.repeat == 0)
	{
		//Adjust the velocity
		switch (e.key.keysym.sym)
		{
		case SDLK_UP:		setVelY(getVelY() + VEL); break;
		case SDLK_DOWN:		setVelY(getVelY() - VEL); break;
		case SDLK_LEFT:		setVelX(getVelX() + VEL); break;
		case SDLK_RIGHT:	setVelX(getVelX() - VEL); break;
		}
	}
	return false;
}

bool Ship::quitLife(const uint8_t damage){
	if ((mLife -= damage) > 0){
		return false;
	}
	else{
		return true;
	}
}