#include "Bullet.h"


Bullet::Bullet(int x, int y, int velX, int velY, uint8_t Power, uint8_t TextureNumber)
	: Primitive(x, y, velX, velY, TextureNumber)
{
	mPower = Power;
}

Bullet::~Bullet()
{
}

uint8_t Bullet::getPower()const{
	return mPower;
}