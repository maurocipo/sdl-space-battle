#ifndef SHIP_H
#define SHIP_H

#include "ObjectPrimitives.h"

class Ship : public Primitive
{
public:
	Ship(const int X, const int Y, const int VelX, const int VelY, int16_t Life, uint8_t TextureNumber, bool aliade);
	~Ship();

	//Takes key presses and adjusts the ship's velocity
	bool handleEvent(SDL_Event& e);

	// Takes damage from life. If life <=0, returns true. Indicates ship is dead.
	bool quitLife(const uint8_t damage);

private:
	int16_t mLife;


};
#endif