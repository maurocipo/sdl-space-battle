#ifndef OBJECTPRIMITIVES_H
#define OBJECTPRIMITIVES_H

#include <SDL.h>
#include <vector>

class Primitive
{
public:
	// A constructor to specify every parameter.
	// For Bullets
	Primitive(const int X, const int Y, const int VelX, const int VelY, const uint8_t TextureNumber);

	// For Ships
	Primitive(const int X, const int Y, const int VelX, const int VelY, const uint8_t TextureNumber, const bool aliade);
	~Primitive();

	//Moves the ship. If it went out of limits, return true.
	bool move();

	/**
	* Check collition between two ships
	*/
	bool checkCollision(Primitive& a) const;

	int getX() const;
	int getY() const;
	uint8_t getFace() const;
	uint8_t getTexNum() const;
	int getWidth() const;
	int getHeight() const;
	int getVelX() const;
	int getVelY() const;

	void setX(const int X);
	void setY(const int Y);
	void setFace(const uint8_t Face);
	void setWidth(const int width);
	void setHeight(const int height);
	void setVelX(const int velX);
	void setVelY(const int velY);

private:
	// Position
	int mPosX, mPosY;
	// Velocity
	int mVelX, mVelY;
	// Facing direction
	uint8_t mFace;
	// Ship size
	int mWith, mHeight;
	// Indicates which texture to render from the corresponding vector
	uint8_t mTextureNumber;
	// Indicates if it is an aliade ship. Used in move().
	bool mAliade;
	// Indicates if the enemy moves in circular way or not.
	bool mCircularMovement;
	// Takes count of ticks fot circular movements.
	int mTicks;
};

#endif