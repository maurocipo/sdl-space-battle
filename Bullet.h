#ifndef BULLET_H
#define BULLET_H

#include "ObjectPrimitives.h"

class Bullet : public Primitive
{
public:
	// A constructor to specify every parameter, and receives a renderer, so it loads the texture. Also sets width and height.
	Bullet(const int X, const int Y, const int VelX, const int VelY, uint8_t Power, uint8_t TextureNumber);

	~Bullet();

	uint8_t getPower()const;

private:
	uint8_t mPower;
};

#endif