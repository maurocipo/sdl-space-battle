#include "Auxiliary.h"

#include <iostream>

int gameExec(){
	srand(time(nullptr));

	// Create window, renderer and font that will be initialized in SDLInit function.
	SDL_Window *win = nullptr;
	SDL_Renderer *ren = nullptr;
	TTF_Font *gFont = nullptr;

	if (SDLInit(&win, &ren, &gFont))
	{
		// Background height for scrolling.
		int backHeight;
		// Loads the bmps to memory and transforms it into a texture.
		SDL_Texture *background = loadTexture("imgs/background1024.jpg", ren);
		SDL_QueryTexture(background, nullptr, nullptr, nullptr, &backHeight);

		// Explosion Texture
		SDL_Texture *ExplosionT = loadTexture("imgs/Explosion.png", ren);
		//Explosions sprites
		SDL_Rect exSpriteClips[EXPLOSION_FRAMES];
		// Vector that contains positions of Explosions to destroy
		std::vector<int> VExDestroyer;
		int ExplosionPosition = 0;

		// Smoke Texture
		SDL_Texture *SmokeT = loadTexture("imgs/blueSmoke.png", ren);
		//Smoke sprites
		SDL_Rect SmokeSpriteClips[SMOKE_FRAMES];

		// Propulsion Texture
		SDL_Texture *PropulsionT = loadTexture("imgs/blueFire.png", ren);
		// Propulsion Sprites
		SDL_Rect PropulsionSpriteClips[PROPULSION_FRAMES];

		// FrontFire Sprites
		SDL_Rect FrontFireSpriteClips[FRONTFIRE_FRAMES];
		SDL_Texture *FrontFireBullet = loadTexture("imgs/frontFireBullet.jpg", ren);
		uint8_t fireFrames = 0;

		loadSprites(exSpriteClips, SmokeSpriteClips, PropulsionSpriteClips, FrontFireSpriteClips);

		// Aliade Textures.
		SDL_Texture *Aliade[ALIADE_FRAMES];

		// Vector that contains all enemy textures
		std::vector<SDL_Texture*> EnemiesTexs;

		// Vector that contains all bullets textures
		std::vector<SDL_Texture*> WeaponsTexs;

		// Load them, and have them on their vector
		loadTexs(Aliade, EnemiesTexs, WeaponsTexs, ren);

		// Text Texture
		SDL_Texture *Text = nullptr;

		// Check Failures
		if (background == nullptr || SmokeT == nullptr || PropulsionT == nullptr || Aliade == nullptr){
			SDL_DestroyTexture(background);
			SDL_DestroyTexture(SmokeT);
			SDL_DestroyTexture(PropulsionT);
			for (uint8_t i = 0; i < ALIADE_FRAMES; ++i){
				SDL_DestroyTexture(Aliade[i]);
			}
			SDL_DestroyRenderer(ren);
			SDL_DestroyWindow(win);
			SDL_Quit();
			return 1;
		}

		// Event handler.
		SDL_Event e;

		// Vector that contains all enemies to iterate and move them.
		std::vector<Ship*> VEnemies;

		// Vector that contains all explosions
		std::vector<Explosion*> VExplosions;

		// Vector that contains bullets
		std::vector<Bullet*> VBullets;

		// Vector that contains smoke particles
		std::list<SmokeParticle*> VSmoke;

		//Current time start time
		uint32_t shootTime = 0;
		uint32_t liveDecreaseTime = 0;
		uint32_t explosionFPS = 0;
		uint32_t smokeFPS = 0;
		uint32_t propulsionFPS = 0;
		uint32_t frontFireFPS = 0;
		uint32_t FPS = 0;

		// Killed enemies.
		uint16_t kills = 0;
		std::string SKills;

		// Player lives
		uint8_t lives = 5;
		std::string SLives;

		// Current Level of the game.
		uint8_t level = 0;

		// Create a description for each weapon and call the function that efectively makes it.
		weaponDescription MissileDesc;
		weaponDescription FrontFireDesc;
		describeWeapons(MissileDesc, FrontFireDesc);

		// Current weapon used. Starts with missiles
		uint8_t weapon = frontFire;

		// Ship's animation.
		uint8_t shipsFace = SHIPS_FACE_TOP;
		//Side where the ship is going. 0 left, 1 centre, 2 right.
		int8_t side = centre;

		// Terminating flag.
		bool quit = false;
		// Flag used to genetate missiles.
		bool shoot = false;
		// Flag used to count crashes.
		bool crash = false;
		// Flag used to draw the front fire.
		bool devanishFire = false;

		// Create the player's aliade.
		int width, height;
		SDL_QueryTexture(Aliade[SHIPS_FACE_TOP], NULL, NULL, &width, &height);
		Ship* aliade = new Ship((SCREEN_WIDTH + width) / 2, SCREEN_HEIGHT - height, 0, 0, 0, 0, true);
		aliade->setHeight(height);
		aliade->setWidth(width);

		// Propulsion object
		SmokeParticle* BlueFire = new SmokeParticle(aliade->getX(), aliade->getY() + height);

		//Start the smoke
		for (uint8_t i = 0; i < SMOKE_PARTICLES; ++i){
			VSmoke.push_back(new SmokeParticle(BlueFire->getX() - 5 + (rand() % 25),
				BlueFire->getY() + -5 + (rand() % 80)));
		}

		//The background scrolling offset
		int scrollingOffset = -(backHeight - SCREEN_HEIGHT);//1100

		// Game Loop.
		while (!quit){

			switch (level)
			{
			case 0:
			{
				//Handle events on queue
				while (SDL_PollEvent(&e) != 0)
				{
					//User requests quit
					if (e.type == SDL_QUIT)
					{
						quit = true;
						side = centre;
						shipsFace = SHIPS_FACE_TOP;

					}
					if (e.type == SDL_KEYUP && (e.key.keysym.sym == SDLK_RETURN)){
						level = 1;
					}
				}
				//First clear the renderer
				SDL_RenderClear(ren);
				//Draw the background
				renderTexture(background, ren, 0, scrollingOffset, NULL);
				loadFromRenderedText("WELCOME TO SPACE BATTLE!", &gFont, &Text, &ren);
				SDL_QueryTexture(Text, NULL, NULL, &width, &height);
				renderTexture(Text, ren, (SCREEN_WIDTH - width) / 2, 20, nullptr);
				loadFromRenderedText("Hit 'Enter' to play", &gFont, &Text, &ren);
				SDL_QueryTexture(Text, NULL, NULL, &width, &height);
				renderTexture(Text, ren, (SCREEN_WIDTH - width) / 2, (SCREEN_HEIGHT - height) / 2, nullptr);
				break;
			}
			case 255:
			{
				//First clear the renderer
				SDL_RenderClear(ren);
				//Draw the background
				renderTexture(background, ren, 0, scrollingOffset, NULL);
				loadFromRenderedText("GAME OVER", &gFont, &Text, &ren);
				SDL_QueryTexture(Text, NULL, NULL, &width, &height);
				renderTexture(Text, ren, (SCREEN_WIDTH - width) / 2, (SCREEN_HEIGHT - height) / 2, nullptr);
				loadFromRenderedText("Hit 'Enter' to play again or 'Esc' to quit", &gFont, &Text, &ren);
				SDL_QueryTexture(Text, NULL, NULL, &width, &height);
				renderTexture(Text, ren, (SCREEN_WIDTH - width) / 2, (SCREEN_HEIGHT - height) / 2 + 35, nullptr);
				//Handle events on queue
				while (SDL_PollEvent(&e) != 0)
				{
					//User requests quit
					if (e.type == SDL_QUIT)
					{
						quit = true;

					}
					if (e.type == SDL_KEYUP && (e.key.keysym.sym == SDLK_RETURN)){
						level = 1;
						lives = 5;
						kills = 0;
						aliade->setVelX(0);
						aliade->setVelY(0);
						SDL_QueryTexture(Aliade[4], NULL, NULL, &width, &height);
						aliade->setX((SCREEN_WIDTH - width) / 2);
						aliade->setY(SCREEN_HEIGHT - height);
					}
					if (e.type == SDL_KEYUP && (e.key.keysym.sym == SDLK_ESCAPE)){
						quit = true;
					}
				}
				VBullets.clear();
				VEnemies.clear();
				VExDestroyer.clear();
				VExplosions.clear();
				shoot = false;
				side = centre;
				shipsFace = SHIPS_FACE_TOP;
				break;
			}
			default:
			{
				if (kills < 20) level = 1;
				else if (kills < 50) level = 2;
				else if (kills < 100) level = 3;

				// Create enemies depending on the level.
				createEnemies(level, ren, VEnemies, EnemiesTexs);

				//Handle events on queue
				while (SDL_PollEvent(&e) != 0)
				{
					if (e.type == SDL_KEYUP && (e.key.keysym.sym == SDLK_ESCAPE)){
						level = 255;
					}
					if (e.key.keysym.sym == SDLK_LEFT){
						side = left;
					}
					if (e.key.keysym.sym == SDLK_RIGHT){
						side = right;
					}
					//User requests quit
					if (e.type == SDL_QUIT)
					{
						quit = true;
						side = centre;
						shipsFace = SHIPS_FACE_TOP;
					}
					// If user releases key, update flag so the ship faces upwards.
					if (e.type == SDL_KEYUP && (e.key.keysym.sym == SDLK_LEFT || e.key.keysym.sym == SDLK_RIGHT))
						side = centre;
					//Handle input for the aliade
					aliade->handleEvent(e);
					// Check if we have to shoot.
					if (e.type == SDL_KEYDOWN && e.key.keysym.sym == SDLK_SPACE){
						shoot = true;
					}
					else if (e.type == SDL_KEYUP && e.key.keysym.sym == SDLK_SPACE){
						shoot = false;
					}

				}

				switch (side)
				{
				case left:
				{
					if (shipsFace - 1 >= SHIPS_FACE_LEFT)
						--shipsFace;
					break;
				}
				case centre:
				{
					if (shipsFace != SHIPS_FACE_TOP)
						if (shipsFace > SHIPS_FACE_TOP)
							--shipsFace;
						else
							++shipsFace;
				}
				break;
				case right:
				{
					if (shipsFace + 1 <= SHIPS_FACE_RIGHT)
						++shipsFace;
					break;
				}
				default:
					break;
				}

				// Shoot if necesary
				
					SDL_QueryTexture(Aliade[SHIPS_FACE_TOP], NULL, NULL, &width, &height);
					switch (weapon)
					{
					case missile:
					{
						if (shoot && ((SDL_GetTicks() - shootTime)) > SHOOT_TIME){
							VBullets.push_back(new Bullet(aliade->getX() + (width / 2), aliade->getY(),
								MissileDesc.velX, MissileDesc.velY, MissileDesc.Power, MissileDesc.textureNumber));
							SDL_QueryTexture(WeaponsTexs.at(VBullets.back()->getTexNum()), NULL, NULL, &width, &height);
							VBullets.back()->setWidth(width);
							VBullets.back()->setHeight(height);
						}
					break;
					}
					case frontFire:
					{
						if (shoot && VBullets.size() == 0){
							VBullets.push_back(new Bullet(aliade->getX() + (width / 2), aliade->getY(),
							FrontFireDesc.velX, FrontFireDesc.velY, FrontFireDesc.Power, FrontFireDesc.textureNumber));
							SDL_QueryTexture(FrontFireBullet, NULL, NULL, &width, &height);
							std::cout << width << " " << height << std::endl;
							VBullets.back()->setWidth(width);
							VBullets.back()->setHeight(height);
						}
						else{
							if (!shoot && devanishFire){
								VBullets.pop_back();
							}
						}
					}
					default:
						break;
					}
					shootTime = SDL_GetTicks();
				

				if (crash && (SDL_GetTicks() - liveDecreaseTime) > LIFE_TAKE_TIME){
					lives -= 1;
					liveDecreaseTime = SDL_GetTicks();
					if (lives == 0) level = 255;
				}

				//Move the ships and bullets and check collisions with aliade.
				crash = moveShipsAndBullets(VEnemies, VBullets, VExplosions, *aliade, kills);

				//Scroll background
				++scrollingOffset;
				if (scrollingOffset > 0){
					scrollingOffset = -(backHeight - SCREEN_HEIGHT);
				}
				//First clear the renderer
				SDL_RenderClear(ren);

				//Draw the background
				renderTexture(background, ren, 0, scrollingOffset, nullptr);

				// Draw the enemies
				for (std::vector<Ship*>::iterator it = VEnemies.begin(); it != VEnemies.end(); ++it){
					renderTexture(EnemiesTexs.at((*it)->getTexNum()), ren, (*it)->getX(), (*it)->getY(), nullptr);
				}

				// Draw the bullets
				if (weapon != frontFire){
					for (std::vector<Bullet*>::iterator it = VBullets.begin(); it != VBullets.end(); ++it){
						renderTexture(WeaponsTexs.at((*it)->getTexNum()), ren, (*it)->getX() - 8, (*it)->getY(), nullptr);
					}
				}
				else{
					if (shoot && !devanishFire){
						renderTexture(WeaponsTexs.at(FrontFireDesc.textureNumber), ren, aliade->getX() + 5,
							aliade->getY() - FrontFireSpriteClips[0].h, &FrontFireSpriteClips[fireFrames]);
						FPS = SDL_GetTicks() - frontFireFPS;
						if (FPS > FRONTFIRE_FPS){
							++fireFrames;
							frontFireFPS = SDL_GetTicks();
						}
						if (fireFrames > 3) fireFrames = 1;
					}

					if (!shoot && fireFrames != 0){
						renderTexture(WeaponsTexs.at(FrontFireDesc.textureNumber), ren, aliade->getX() + 5,
							aliade->getY() - FrontFireSpriteClips[0].h, &FrontFireSpriteClips[fireFrames]);
						
						FPS = SDL_GetTicks() - frontFireFPS;
						if (FPS > FRONTFIRE_FPS){
							++fireFrames;
							frontFireFPS = SDL_GetTicks();
						}
						if (fireFrames == FRONTFIRE_FRAMES) {
							fireFrames = 0;
							devanishFire = false;
						}
					}

					if (lives == 0){
						fireFrames = 0;
						devanishFire = false;
					}
				}
				//Draw the aliade
				renderTexture(Aliade[shipsFace], ren, aliade->getX(), aliade->getY(), nullptr);

				//Draw the propulsion fire
				FPS = SDL_GetTicks() - propulsionFPS;
				BlueFire->setX(aliade->getX() + 18);
				BlueFire->setY(aliade->getY() + aliade->getHeight() - 4);
				renderTexture(PropulsionT, ren, BlueFire->getX(), BlueFire->getY(), &PropulsionSpriteClips[BlueFire->getFrame()]);
				if (FPS > PROPULSION_FPS){
					BlueFire->addFrame();
					propulsionFPS = SDL_GetTicks();
				}
				if (BlueFire->getFrame() > (PROPULSION_FRAMES - 1)){
					BlueFire->setFrame(0);
				}

				// Draw the Explosions
				FPS = SDL_GetTicks() - explosionFPS;
				for (std::vector<Explosion*>::iterator it = VExplosions.begin(); it != VExplosions.end(); ++it){
					renderTexture(ExplosionT, ren, (*it)->getX(), (*it)->getY(), &exSpriteClips[(*it)->getFrame()]);
					if (FPS > EXPLOSIONS_FPS){
						(*it)->addFrame();
						explosionFPS = SDL_GetTicks();
					}
					if ((*it)->getFrame() > (EXPLOSION_FRAMES - 1)){
						VExDestroyer.insert(VExDestroyer.begin(), ExplosionPosition);
					}
					++ExplosionPosition;
				}
				for (std::vector<int>::iterator it = VExDestroyer.begin(); it != VExDestroyer.end(); ++it){
					delete(*(VExplosions.begin() + (*it)));
					VExplosions.erase(VExplosions.begin() + (*it));
				}
				ExplosionPosition = 0;
				VExDestroyer.clear();



				// Draw the smoke
				FPS = SDL_GetTicks() - smokeFPS;
				for (std::list<SmokeParticle*>::iterator it = VSmoke.begin(); it != VSmoke.end(); ++it){

					renderTexture(SmokeT, ren, (*it)->getX(), (*it)->getY(), &SmokeSpriteClips[(*it)->getFrame()]);
					if (FPS > SMOKE_FPS){
						(*it)->addFrame();
						smokeFPS = SDL_GetTicks();
					}
					if ((*it)->getFrame() > (SMOKE_FRAMES - 1)){

						(*it)->setX(BlueFire->getX() - 5 + (rand() % 25));
						(*it)->setY(BlueFire->getY() + 40 + (rand() % 80));
						(*it)->setFrame(rand() % (SMOKE_FRAMES-1));
					}

				}

				//Draw Text
				SKills = std::to_string(kills);
				SLives = std::to_string(lives);
				loadFromRenderedText("Lives:" + SLives, &gFont, &Text, &ren);
				renderTexture(Text, ren, 5, SCREEN_HEIGHT - 65, nullptr);
				loadFromRenderedText("Kills: " + SKills, &gFont, &Text, &ren);
				renderTexture(Text, ren, 5, SCREEN_HEIGHT - 35, nullptr);
				break;
			}
			}
			//Update the screen
			SDL_RenderPresent(ren);
		}

		SDL_DestroyTexture(background);
		for (std::vector<SDL_Texture*>::iterator it = EnemiesTexs.begin(); it != EnemiesTexs.end(); ++it){
			SDL_DestroyTexture(*it);
		}
		for (std::vector<SDL_Texture*>::iterator it = WeaponsTexs.begin(); it != WeaponsTexs.end(); ++it){
			SDL_DestroyTexture(*it);
		}
		SDL_DestroyTexture(ExplosionT);
		SDL_DestroyTexture(Text);
		//	SDL_DestroyTexture(ShipT);
		//	SDL_DestroyTexture(ShipL);
		//	SDL_DestroyTexture(ShipR);
		SDL_DestroyRenderer(ren);
		SDL_DestroyWindow(win);
		SDL_Quit();
		return 0;
	}
	else{
		std::cout << "Could NOT Initialize SDL correctly. Bad Luck. Bye!" << std::endl;
		return 0;
	}
};


int main(int argc, char* args[]){

	return gameExec();

}
