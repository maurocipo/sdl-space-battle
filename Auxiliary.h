#ifndef AUX_H
#define AUX_H

#include <SDL.h>
#include <SDL_image.h>
#include <SDL_ttf.h>
#include <string>
#include <time.h>
#include <vector>
#include <list>

#include "Ship.h"
#include "Bullet.h"
#include "Explosion.h"
#include "SmokeParticle.h"

const int SCREEN_WIDTH = 1024;
const int SCREEN_HEIGHT = 768;

// Size of Aliade Textures array. Current Sprite is 11 frames
#define ALIADE_FRAMES 11
// Amount of SDL_Rects necessary to divide the Explosion Sprite
#define EXPLOSION_FRAMES 49
// Amount of SDL_Rects necessary to divide the Propulsion Sprite
#define PROPULSION_FRAMES 4
// Amount of SDL_Rects necessary to divide the Smoke Sprite
#define SMOKE_FRAMES 16
// Amount of SDL_Rects necessary to divide the FrontFire Sprite
#define FRONTFIRE_FRAMES 18

// Extra time given to the player in case he crashes an enemy.
#define LIFE_TAKE_TIME 500
// Time for reloading
#define SHOOT_TIME 100

// FPS to draw propulsion fire.
#define PROPULSION_FPS 10
// FPS to draw each explosion.
#define EXPLOSIONS_FPS 40
// FPS to draw smoke.
#define SMOKE_FPS 40
// FPS to draw frontFire
#define FRONTFIRE_FPS 30

// Quantity of smoke particles tailing the ship
#define SMOKE_PARTICLES 50

// Ships faces to control drawing.
#define SHIPS_FACE_TOP 5
#define SHIPS_FACE_LEFT 0
#define SHIPS_FACE_RIGHT 10
// Ships going-side to control drawing
enum shipsSide{
	left,
	centre,
	right
};

// Types of weapons
enum weapons{
	missile,
	frontFire
};

// weaponDescription struct. In main there is one for each weapon.
struct weaponDescription {
	int velX;
	int velY;
	uint8_t Power;
	uint8_t textureNumber;
};

// Maximun Velocity for Aliade
#define VEL 20

/**
* Initialize all SDL systems and create de window and renderer.
*/
bool SDLInit(SDL_Window **win, SDL_Renderer **ren, TTF_Font **gFont);

/**
* Log an SDL error with some error message to the output stream of our choice
* @param os The output stream to write the message to
* @param msg The error message to write, format will be msg error: SDL_GetError()
*/
void logSDLError(std::ostream &os, const std::string &msg);

/**
* Loads an image into a texture on the rendering device
* @param file The image file to load
* @param ren The renderer to load the texture onto
* @return the loaded texture, or nullptr if something went wrong.
*/
SDL_Texture* loadTexture(const char* file, SDL_Renderer *ren);

/**
* Draw an SDL_Texture to an SDL_Renderer at position x, y, with some desired
* width and height
* @param tex The source texture we want to draw
* @param ren The renderer we want to draw to
* @param x The x coordinate to draw to
* @param y The y coordinate to draw to
* @param w The width of the texture to draw
* @param h The height of the texture to draw
*/
void renderTexture(SDL_Texture *tex, SDL_Renderer *ren, int x, int y, SDL_Rect *clip);

/**
* Creates image from font string
*/
void loadFromRenderedText(std::string textureText, TTF_Font **gFont, SDL_Texture **Text, SDL_Renderer** ren);

/**
* Load Textures
*/
void loadTexs(SDL_Texture *Aliade[], std::vector<SDL_Texture*> &EnemiesTexs, std::vector<SDL_Texture*> &WeaponsTexs,
	SDL_Renderer *ren);

/**
* Load weapons descriptions
*/
void describeWeapons(weaponDescription& MissileDesc, weaponDescription& FrontFire);

/**
* Move and destroy ships and bullets. Returns True if player lost.
*/
bool moveShipsAndBullets(std::vector<Ship*>& VEnemies, std::vector<Bullet*>& VBullets, std::vector<Explosion*>& VExplosions,
	Ship& aliade, uint16_t& kills);

/**
* Create Enemies depending on the level
*/
void createEnemies(uint8_t level, SDL_Renderer *ren, std::vector<Ship*>& VEnemies, std::vector<SDL_Texture*> &EnemiesTexs);

/**
* Calculates Sprites Clips
*/
void loadSprites(SDL_Rect(&exSpriteClips)[49], SDL_Rect(&SmokeSpriteClips)[16], SDL_Rect PropulsionSpriteClips[4],
	SDL_Rect(&FrontFireSpriteClips)[18]);

#endif